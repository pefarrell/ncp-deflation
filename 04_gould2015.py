from solver import SemismoothNewtonSolver

def residual(x):

    F = [0.0]*4

    F[0] = -4*(x[0] - 0.25) + 3*x[2] + x[3]
    F[1] = +4*(x[1] - 0.50) + x[2] + x[3]
    F[2] = 1.5 - 3*x[0] - x[1]
    F[3] = 1.0 - x[0]   - x[1]

    return F

x_init = [0.3]*4
solver = SemismoothNewtonSolver(residual, power=2.0, shift=1.0)

while solver.convergence_reason > -1:
    x = solver.solve(x_init)
    if solver.convergence_reason > 0:
        print "-" * 80
        print "--- Attempting deflation; stand back, citizen!"
        print "-" * 80

solver.print_results()
