from solver import SemismoothNewtonSolver
from numpy import array, arange, zeros, dot, set_printoptions, nan

options = {'max_it' : 15, 'simple_linesearch': True}

N = 42
q = zeros(N)
M = zeros((N, N))
for line in open("05_tinloi%s.dat" % N):
    if line[0] == "i" and line[8] == "i":
        s = line.split()
        i = int(s[0][1:]) - 1
        j = int(s[2][1:]) - 1
        val = float(s[3])
        M[i,j] = val

for line in open("05_tinloi%s.dat" % N):
    if line[0] == "i" and line[8] != "i":
        s = line.split()
        i = int(s[0][1:]) - 1
        val = float(s[1])
        q[i] = val

def residual(x):
    F = dot(M, x) + q
    return F

x_init = [0.4]*N 
solver = SemismoothNewtonSolver(residual, power=1.0, shift=1.0, options=options)

while solver.convergence_reason > -1:
    x = solver.solve(x_init)
    if solver.convergence_reason > 0:
        print "-" * 80
        print "--- Attempting deflation; stand back, citizen!"
        print "-" * 80

solver.print_results()
