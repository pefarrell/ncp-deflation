"""
Kojima's nonlinear complementarity problem with two known solutions.

See

@article{dirske1995,
author = {Dirkse, S. P. and Ferris, M. C.},
title = {{MCPLIB}: a collection of nonlinear mixed complementarity problems},
journal = {Optimization Methods and Software},
volume = {5},
number = {4},
pages = {319--345},
year = {1995},
doi = {10.1080/10556789508805619},
}

"""

from solver import SemismoothNewtonSolver

def residual(x):
    F = [0]*4

    F[0] = 3*x[0]**2 + 2*x[0]*x[1] + 2*x[1]**2        +    x[2] + 3*x[3] - 6
    F[1] = 2*x[0]**2               +   x[1]**2 + x[0] + 10*x[2] + 2*x[3] - 2
    F[2] = 3*x[0]**2 +   x[0]*x[1] + 2*x[1]**2        +  2*x[2] + 9*x[3] - 9
    F[3] =   x[0]**2               + 3*x[1]**2        +  2*x[2] + 3*x[3] - 3

    return F

x_init = [2.0]*4
solver = SemismoothNewtonSolver(residual, power=1.0, shift=0.5)

while solver.convergence_reason > -1:
    x = solver.solve(x_init)
    if solver.convergence_reason > 0:
        print "-" * 80
        print "--- Attempting deflation; stand back, citizen!"
        print "-" * 80

solver.print_results()
