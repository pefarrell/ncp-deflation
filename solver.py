#!/usr/bin/python
# -*- coding: utf-8 -*-

from numpy import array, zeros, zeros_like, ones_like, exp
from numpy import sqrt, finfo, diagflat, inner
from numpy.linalg import norm, solve as solvesys, LinAlgError

from copy import copy, deepcopy

def vecarray(x): return [float(x[i]) for i in range(len(x))]

def project(x): return array([max(y,0.0) for y in vecarray(x)])

def chi(x, delta):
    # Compactly supported analytic test function.
    s = 0.0
    r = sum((x[i])**2 for i in range(len(x)))
    if r < delta:
        try: s = exp(1.0)*exp((delta/(r-delta)))
        except AttributeError:
            # uncertainties apparently does not support numpy.exp:
            # need to wrap the exponential function
            from uncertainties import wrap
            wrapped_exp = wrap(exp)
            s = wrapped_exp(1.0)*wrapped_exp((delta/(r-delta)))
    return s

def eta(x, r): 
    # Compute the norm of the distance between x and r.
    return sum((x[i] - r[i])**2 for i in range(len(x)))**0.5

def nu(x, r):
    Psi = ones_like(x)
    for j in range(len(x)):
        Psi[j] *= sum((x[i] - r[i])**2 for i in range(len(x)) if i != j )**0.5
    return Psi

class SemismoothNewtonSolver(object):
    def __init__(self, residual, power, shift, options=None):
        self.p = float(power)
        self.shift = float(shift)
        self.F = residual
        self.true_residual = deepcopy(residual)
        self.roots = {'ZeroResidual' : [], 'AllRoots' : []}
        self.convergence_reason = 0
        self.setup(options)

    def setup(self, options=None):
        # Absolute tolerance
        self.atol = 1.0e-12
        # Maximum iteration number
        self.max_it = 50
        # Support radius of the analytic test function
        self.delta = 1.0e-6
        # Simple backtracking projected Armijo linesearch flag
        self.simple_linesearch = False
        # Purification flag
        self.purification = False

        self.F = self.deflate_residual(deepcopy(self.F))
        self.jacobian = self.make_jacobian(self.F)

        if options is not None:
            for key, value in options.iteritems():
                setattr(self, key, value)

        if self.purification == False:
            self.delta = max(self.delta, 5.0*self.atol)

    def deflate_residual(self,residual):
        def deflated_residual(x):
            deflation_factor = 1.0
            res = array(residual(x))
            for root in self.roots['ZeroResidual']:
                # If the residual is zero at the root, then deflate the residual
                if norm(self.true_residual(root)) <= min(1.0e2*self.atol, 5.0e-6):
                    eval_eta = eta(x, root)**self.p
                    assert eval_eta != 0.0
                    deflation_factor *= 1.0/eval_eta + self.shift
                    assert deflation_factor != 0.0
            return res*deflation_factor

        return deflated_residual

    def deflated_position_vector(self,X):
        deflation_factor = 1.0
        s = zeros_like(X)
        for root in self.roots['AllRoots']:
            root = array(root)
            # if the point is not internal, then deflate the residual
            if sum([abs(y) < 1.0e-8 for y in root]) > 0:
                eval_eta = eta(X, root)**self.p
                assert eval_eta != 0.0
                #s += nu(X, root)**self.p * chi(X - root, self.delta)
                s += chi(X - root, self.delta)
                deflation_factor *= 1.0/eval_eta
        if len(self.roots['AllRoots'])>0:
            return (array(X) + s)*deflation_factor + self.shift*array(X)
        else:
            return array(X)

    def make_jacobian(self,residual):
        import uncertainties
        from uncertainties import unumpy
        def J(x):
            N = len(x)
            x = [uncertainties.ufloat(float(x[i]), 0.0) for i in range(N)]
            F = residual(x)
            out = zeros((N, N))
            for i in range(N):
                for j in range(N):
                    try:
                        out[i,j] = F[i].derivatives[x[j]]
                    except KeyError:
                        out[i,j] = 0.0
            return out
        return J

    def print_results(self):
        info_blue("Roots found: %d" % len(self.roots['AllRoots']))
        for (i, root) in enumerate(self.roots['AllRoots']):
                info_green("Root %3d: %s" % (i, root))

    def purify(self):
        roots = copy(self.roots)
        self.purification = False
        self.atol = 1.0e-3*self.atol
        purified_roots = {'ZeroResidual' : [], 'AllRoots' : []}
        for root in roots['AllRoots']:
            self.roots = {'ZeroResidual' : [], 'AllRoots' : []}
            purified_roots['AllRoots'].append(self.solve(root))
        self.atol = 1.0e3*self.atol
        self.purification = True
        self.roots = purified_roots

    def solve(self, x_init, monitor_residual=True, monitor_iterates=False):

        # epsilon machine
        e_mach = finfo(float).eps

        # Fischer-Burmeister Merit Function
        # Need to solve phi_FB(X,F) == 0
        def phi_FB(a,b):
            out = zeros_like(a)
            DPV = self.deflated_position_vector(a)
            for i in range(len(out)):
                s = DPV[i] + b[i]
                s_abs = abs(DPV[i]) + abs(b[i])
                if s_abs <= e_mach:
                    sqroot = 0.0
                else:
                    sqroot = s_abs*sqrt((DPV[i]/s_abs)**2 + (b[i]/s_abs)**2)
                if s <= 0:
                    out[i] = sqroot - s
                else:
                    out[i] = -2.0*DPV[i]*b[i]/(sqroot + s)
            return out

        # Handling inputs
        Xk = array(x_init)

        # Initial value of the residual
        Fk = self.F(Xk)

        # Initial value of the position vector residual
        DPV = self.deflated_position_vector(Xk)

        # Position vector jacobian
        pvjacobian = self.make_jacobian(self.deflated_position_vector)

        # Get Problem Size
        N = Xk.size

        # Solver parameters
        # Sufficient decrease parameters (modified angle condition)
        rho = 1.0e-10
        q = 2.1
        # Backtracking Armijo condition parameters:
        # beta = multiply the step size by beta if the Armijo condition is not satisfied to make the step shorter
        # tau = fixed Armijo condition parameter. See https://en.wikipedia.org/wiki/Backtracking_line_search
        beta = 0.5
        tau = 1.0e-4
        # Absolute tolerance
        atol = self.atol
        # Maximum iteration number
        max_it = self.max_it

        # Initialise B-subdifferential diagonal matrices
        D_a = zeros_like(Xk)
        D_b = zeros_like(Xk)

        # Initialise descent direction
        d = ones_like(Xk)

        # Initialise jacobian of x/||x||^p for corner deflation
        # if p == 0 then corner_jacobian = identity
        # if p > 0 then corner_jacobian = shift*I + (identity - p*(v*v^T))/orx||^p, where v = x/||x||
        # in the second case Sherman-Woodbury-Morrison formula could be used for cheap inverse,
        # but this is not necessary for small problems

        # Main Loop 
        for k in range(max_it):

            # Steepest Descent Flag
            simple_linesearch_flag = False

            # Evaluating FB function
            phi_k = phi_FB(Xk,Fk)

            # Check for convergence
            phi_norm = norm(phi_k)
            if monitor_residual:
                info_bold(' %3d |Ψ|: %.15e' % (k, phi_norm))

            # Added check on the inner product to ensure convergence has occured
            if phi_norm < atol and norm(Xk*self.true_residual(Xk)) < 1.0e2*atol:
                self.convergence_reason = 1
                self.roots['AllRoots'].append(vecarray(Xk))
                if norm(self.true_residual(Xk)) <= 5.0e-6:
                    self.roots['ZeroResidual'].append(vecarray(Xk))
                info_green('Nonlinear solve converged due to absolute tolerance %.15e < %.15e' % (phi_norm, atol))
                return Xk


            # Evaluating the Jacobian
            J = self.jacobian(Xk)
            JP = pvjacobian(Xk)

            # Extracting an element from the B-subdifferential
            norm_vals = sqrt(DPV**2 + Fk**2)
            for i in range(N):
                if norm_vals[i] > min(atol , 1.0e2*e_mach): # This is just to check that norm_vals[i] > 0
                    D_a[i] = DPV[i]/norm_vals[i] - 1.0
                    D_b[i] = Fk[i]/norm_vals[i] - 1.0
                else:
                    D_a[i] = 1/sqrt(2.0) - 1.0
                    D_b[i] = 1/sqrt(2.0) - 1.0

            # Dynamically choosing epsilon
            eps = min(0.5*phi_norm**2, 1.0e-2)/(norm(JP,1) + norm(J,1))

            # B-subdifferential element H,
            # H = diag(D_a)*corner_jacobian + diag(D_b)*J

            # FIXME Rarely, a LinAlgError is raised, complaining about singular matrices.
            # I think this is because the initial condition is a solution that we have deflated, but I have not checked.
            # Finding next step
            singular_matrix = 0
            try:
                # Solving H*d = -phi_k
                H = diagflat(D_a).dot(JP) + diagflat(D_b).dot(J)
                d = solvesys(H, -phi_k)
            except LinAlgError:
                info_red('Warning: Matrix singular to working precision')
                singular_matrix = 1

            # Computing the gradient of 1/2*norm(phi_k)^2
            # by multiplying H^T*phi_k
            grad_psi_k = JP.T.dot(D_a*phi_k) + J.T.dot(D_b*phi_k)

            # Check if the descent test is satisfied
            if inner(grad_psi_k,d) > -rho*norm(d)**q or singular_matrix == 1:
                # Descent test not satisfied: steepest descent step
                d_g = -grad_psi_k
                simple_linesearch_flag = True
            else:
                if self.simple_linesearch == True:
                    d_g = array(d)
                    simple_linesearch_flag = True
                else:
                    d_g = -grad_psi_k

            # Backtracking Projected Armijo
            i = 0
            flag = False
            while flag == False:
                if simple_linesearch_flag == True:
                    XX = project(Xk + beta**i*d_g)
                else:
                    Pd_g = project(Xk + beta**i*d_g) - Xk
                    Pd = project(Xk + beta**i*d) - Xk
                    v = H.dot(Pd_g - Pd)
                    if norm(v) <= atol:
                        XX = project(Xk + beta**i*d)
                    else:
                        tk = -inner(phi_k + H.dot(Pd), v)/norm(v)**2.0
                        tk = max(0.0, min(1.0, tk))
                        XX = Xk + tk*Pd_g + (1.0 - tk)*Pd

                RHS = 0.5*phi_norm**2 + tau * inner(grad_psi_k,project(Xk + beta**i*d_g) - Xk)
                flag = 0.5*norm(phi_FB(XX,self.F(XX)))**2 <= RHS
                if beta**i < e_mach:
                    info_red('Nonlinear solve did not converge due to breakdown of step size')
                    if self.purification == True:
                        self.purify()
                    self.convergence_reason = -2
                    return Xk

                i = i + 1

            # Updating Xk
            Xk = project(XX)

            # Next iterate residual evaluation
            Fk = self.F(Xk)
            DPV = self.deflated_position_vector(Xk)
            if monitor_iterates:
                info_blue( "     Position vector %s" % list(Xk))
                info_red(  "     Deflated pos vec %s" % list(DPV))
                info_green("     True Residual %s" % list(self.true_residual(Xk)))
                info_red(  "     Deflated Residual %s" % list(Fk))

        info_red("Nonlinear solve did not converge due to reaching maximum iterations\n")
        if self.purification == True:
            info_blue("Activating purification")
            self.purify()
        self.convergence_reason = -1
        return Xk

# Auxiliary material for coloured output
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def info_red(msg):
    print bcolors.FAIL + msg + bcolors.ENDC
def info_green(msg):
    print bcolors.OKGREEN + msg + bcolors.ENDC
def info_blue(msg):
    print bcolors.OKBLUE + msg + bcolors.ENDC
def info_bold(msg):
    print bcolors.BOLD + msg + bcolors.ENDC
