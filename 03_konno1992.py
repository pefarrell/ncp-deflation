from solver import SemismoothNewtonSolver
from numpy import array, dot, zeros_like, zeros

options = {'max_it' : 30}

A = array([
          [-0.2, -0.4],
          [0.28, -0.28],
          [0.35, 0.35],
          [0.56, 0.28],
          [0.583333333333333, 0.0],
          [-0.430769230769231, 0.107692307692308],
          [-0.451612903225806, -0.225806451612903],
          ])
v = array([1.2, 0.84, 0.7, 0.56, 0.583333333333333, 1.292307692307692, 1.354838709677419])
def residual(x):

    trasl = 5.0 # the translation to convert to an NCP
    F = [0]*9

    ATlambda = dot(A.T, x[2:9])

    F[0] = +2*(x[0]-trasl) + ATlambda[0]
    F[1] = -2*(x[1]-trasl) + ATlambda[1]
    F[2:9] = v - dot(A, (array(x[0:2])-trasl))

    return F

x_init = [0.0]*9
x_init[0] = 1.0/10
x_init[1] = 36.0/10

solver = SemismoothNewtonSolver(residual, power=1.0, shift=0.5, options=options)

while solver.convergence_reason > -1:
    x = solver.solve(x_init)
    if solver.convergence_reason > 0:
        print "-" * 80
        print "--- Attempting deflation; stand back, citizen!"
        print "-" * 80

solver.print_results()
