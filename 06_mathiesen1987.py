from solver import SemismoothNewtonSolver

options = {'max_it': 20, 'delta': 1.0e-8, 'atol': 1.0e-9}

gamma = 1.0
def residual(x):

    F = [0]*4

    F[0] = -x[1] + x[2] + x[3]
    F[1] =  x[0] - 0.75*(x[2] + gamma*x[3])/x[1]
    F[2] = -x[0] - 0.25*(x[2] + gamma*x[3])/x[2] + 1
    F[3] = -x[0] + gamma

    return F

x_init = [15.0]*4
solver = SemismoothNewtonSolver(residual, power=1.0, shift=1.0, options=options)
zero = [0.0]*4
solver.roots['AllRoots'].append(zero)

while solver.convergence_reason > -1:
    x = solver.solve(x_init)
    if solver.convergence_reason > 0:
        print "-" * 80
        print "--- Attempting deflation; stand back, citizen!"
        print "-" * 80

del solver.roots['AllRoots'][0] # remove the artificial deflation of zero
solver.print_results()
