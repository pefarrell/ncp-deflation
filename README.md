## Introduction

This code supports the paper, "Distinct solutions of finite-dimensional
complementarity problems".

We include our implementation of a semismooth Newton solver that incorporates
deflation techniques to identify multiple solutions, as well as the examples
discussed in the paper.

Thanks for looking at our code!

## Dependencies

This code depends on the uncertainties package by Eric Lebigot:
https://pythonhosted.org/uncertainties/.

We've included a copy of version 2.4.6.1 in our distribution, to minimise
installation headaches.

If numpy built-in functions are necessary in the residual
it is important to use the associated uncertainties.unumpy functions instead,
e.g. from uncertainties.unumpy import sin, cos, log, ...

## Contributors

    Matteo Croci    <matteo.croci@maths.ox.ac.uk>
    Patrick Farrell <patrick.farrell@maths.ox.ac.uk>

## License

This code is released under the LGPL v3.