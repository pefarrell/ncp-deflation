from solver import SemismoothNewtonSolver
from numpy import array, dot, zeros_like 

A = array([[30, 20], [10, 25]])
B = array([[30, 10], [20, 25]])
e = array([1, 1])

def residual(X):
    x = X[0:2]
    y = X[2:4]
    out = zeros_like(X)
    out[0:2] = dot(A, y) - e
    out[2:4] = dot(B.T, x) - e
    return out

x_init = [0.0, 0.0, 0.0, 1.0/30]
solver = SemismoothNewtonSolver(residual, power=1.0, shift=1.0)

while solver.convergence_reason > -1:
    x = solver.solve(x_init)
    if solver.convergence_reason > 0:
        print "-" * 80
        print "--- Attempting deflation; stand back, citizen!"
        print "-" * 80

solver.print_results()
